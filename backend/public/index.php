<?php
try {

    //Register an autoloader
    $loader = new \Phalcon\Loader();
    $loader->registerDirs(
            array(
                '../app/controllers/',
                '../app/models/',
                '../app/plugins/'
            )
    )->register();

    $di = new Phalcon\DI\FactoryDefault();

    //Set the database service
    $di->set('db', function() {
        return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
            "host" => "localhost",
            "username" => "pink",
            "password" => "pink",
            "dbname" => "pinkCrocodile",
            'charset' => 'utf8'
        ));
    });

    //Setup a base URI 
    $di->set('url', function() {
        $url = new \Phalcon\Mvc\Url();
        $url->setBaseUri('/pinkcrocodile/backend/');
        return $url;
    });

    //Setting up the view component
    $di->set('view', function() {
        $view = new \Phalcon\Mvc\View();
        $view->setViewsDir('../app/views/');
        return $view;
    });

    $di->set('view', function() {

        $view = new Phalcon\Mvc\View\Simple();

        $view->setViewsDir('../app/views/');

        return $view;
    }, true);

    $di->set('security', function() {
        $security = new Phalcon\Security();
        //Set the password hashing factor to 12 rounds
        $security->setWorkFactor(12);
        return $security;
    }, true);

    $di->set('dispatcher', function() use ($di) {
        $eventsManager = $di->getShared('eventsManager');
        $security = new Security($di);
        $eventsManager->attach('dispatch', $security);
        $dispatcher = new Phalcon\Mvc\Dispatcher();
        $dispatcher->setEventsManager($eventsManager);
        return $dispatcher;
    });

    $di->setShared('session', function() {
        $session = new Phalcon\Session\Adapter\Files();
        $session->start();
        return $session;
    });

    $app = new Phalcon\Mvc\Micro($di);

    $app->notFound(function () use ($app) {
        $app->response->setStatusCode(404, "Not Found")->sendHeaders();
        echo 'This page was not found!';
    });
/////////////////////////////////////////////////////////////
//-------------------------CATEGORIES----------------------//
/////////////////////////////////////////////////////////////

/*
GET /categories{?lang}
GET /categories/{id}{?lang}
POST /categories/{id}
DELETE /categories/{id}*/

////////////
// REST API - GET /categories/{id}
//-------------------------------------------------------
    $app->get('/api/categories/{id:[0-9]+}', function($id) use ($app) {

        $phql = "SELECT C.id, T.cs as id_name FROM Categories C JOIN Translations T ON T.id=C.id_name WHERE C.id=:id:";
        $category = $app->modelsManager->executeQuery($phql, array(
                    'id' => $id
                ))->getFirst();

        //echo $word->id_name;

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($category == false) {
            $response->setStatusCode(409, "Conflict");
        } else {
            $response->setStatusCode(200, "Ok");

            $response->setJsonContent(array(
                'id' => $category->id,
                'name' => $category->id_name
            ));
        }

        return $response;
    });

    $app->get('/api/categories', function() use ($app) {

        $phql = "SELECT C.id, T.cs as id_name FROM Categories C JOIN Translations T ON T.id=C.id_name";
        $categories = $app->modelsManager->executeQuery($phql);

        $data = array();
        foreach ($categories as $category) {
            $data[] = array(
                'id' => $category->id,
                'name' => $category->id_name
            );
        }

        echo json_encode($data);
    });

/////////////////////////////////////////////////////////////
//-------------------------Rhymes---------------------------//
/////////////////////////////////////////////////////////////
    // REST API - GET /rhymes/{id}
    //-------------------------------------------------------
    $app->get('/api/rhymes/{id:[0-9]+}', function($id) use ($app) {

        $phql = "SELECT * FROM Rhymes WHERE id = :id:";
        $rhyme = $app->modelsManager->executeQuery($phql, array(
                    'id' => $id
                ))->getFirst();

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($rhyme == false) {
            $response->setStatusCode(409, "Conflict");
        } else {
            $response->setStatusCode(200, "Ok");
            $response->setJsonContent(array(
                'id' => $rhyme->id,
                'id_name' => $rhyme->id_name,
                'id_rhyme' => $rhyme->id_rhyme,
                'id_word' => $rhyme->id_word
            ));
        }

        return $response;
    });

    $app->get('/api/rhymes', function() use ($app) {

        $phql = "SELECT * FROM Rhymes";
        $rhymes = $app->modelsManager->executeQuery($phql);

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($rhymes == false) {
            $response->setStatusCode(409, "Conflict");
        } else {
            $response->setStatusCode(200, "Ok");
            $data = array();
            foreach ($rhymes as $rhyme) {
                $data[] = array(
                    'id' => $rhyme->id,
                    'id_name' => $rhyme->id_name,
                    'id_rhyme' => $rhyme->id_rhyme,
                    'id_word' => $rhyme->id_word
                );
            }
            $response->setJsonContent($data);
        }

        return $response;
    });


////////////
// REST API - POST /categories/{id}
//-------------------------------------------------------
/*
$app->post('/api/words', function() use ($app)) {
    
    $category = $app->request->GetJsonRawBody();
       
    $phql = "INSERT INTO Categories (id_name,id_parent) VALUES (:id_name:, :id_parent:)";
       
    $status = $app->modelsManager->executeQuery($phql, array(
            'id_name' => $category->id_name,
            'id_parent' => $category->id_parent
        ));

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($status->success() == true) {
            $response->setStatusCode(201, "Created");
            $category->id = $status->getModel()->id;
            $response->setJsonContent($category);
        } else {

            $response->setStatusCode(409, "Conflict");

            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });

$app->put('/api/categories/{id:[0-9]+}', function($id) use($app) {

        $category = $app->request->getJsonRawBody();

        $phql = "UPDATE Categories SET id_name = :id_name:,img = :id_parent: WHERE id = :id:";
        $status = $app->modelsManager->executeQuery($phql, array(
            'id' => $id,
            'id_name' => $category->id_name,
            'img' => $category->id_parent
        ));

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($status->success() == true) {
            $response->setStatusCode(201, "Created");
            $category->id = $status->getModel()->id;
            $response->setJsonContent($category);
        } else {

            $response->setStatusCode(409, "Conflict");

            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });

*/
////////////
// REST API - DELETE /categories/{id}
//-------------------------------------------------------
/*    $app->delete('/api/categories/{id:[0-9]+}', function($id) use ($app) {

        $phql = "DELETE FROM Categories WHERE id = :id:";
        $status = $app->modelsManager->executeQuery($phql, array(
            'id' => $id
        ));

        $response = new Phalcon\Http\Response();

        if ($status->success() == true) {
            $response->setStatusCode(204, "No Content");
        } else {

            $response->setStatusCode(409, "Conflict");

            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });   
*/
/////////////////////////////////////////////////////////////
//-------------------------USERS---------------------------//
/////////////////////////////////////////////////////////////
    // REST API - GET /users/{id}
    //-------------------------------------------------------
    $app->get('/api/users/{id:[0-9]+}', function($id) use ($app) {

        $phql = "SELECT * FROM Users WHERE id = :id:";
        $user = $app->modelsManager->executeQuery($phql, array(
                    'id' => $id
                ))->getFirst();

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($user == false) {
            $response->setStatusCode(409, "Conflict");
        } else {
            $response->setStatusCode(200, "Ok");
            $response->setJsonContent(array(
                'id' => $user->id,
                'nick' => $user->nick,
                'name' => $user->name,
                'email' => $user->email
            ));
        }

        return $response;
    });

    $app->get('/api/users', function() use ($app) {

        $phql = "SELECT * FROM Users";
        $users = $app->modelsManager->executeQuery($phql);

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($users == false) {
            $response->setStatusCode(409, "Conflict");
        } else {
            $response->setStatusCode(200, "Ok");
            $data = array();
            foreach ($users as $user) {
                $data[] = array(
                    'id' => $user->id,
                    'nick' => $user->nick,
                    'name' => $user->name,
                    'email' => $user->email
                );
            }
            $response->setJsonContent($data);
        }

        return $response;
    });

    //--------------------------------------------------------
    // REST API - POST /users/{id}
    //-------------------------------------------------------

    $app->post('/api/users', function() use ($app) {

        $user = $app->request->getJsonRawBody();

        $phql = "INSERT INTO Users (nick, name, email, passwd, salt, role) VALUES (:nick:, :name:, :email:, :passwd:, :salt:, :role:)";

        $status = $app->modelsManager->executeQuery($phql, array(
            'nick' => $user->nick,
            'name' => $user->name,
            'email' => $user->email,
            'passwd' => $user->passwd,
            'salt' => $user->salt,
            'role' => $user->role
        ));

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($status->success() == true) {
            $response->setStatusCode(201, "Created");
            $user->id = $status->getModel()->id;
            $response->setJsonContent($user);
        } else {

            $response->setStatusCode(409, "Conflict");
            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });

    $app->put('/api/users/{id:[0-9]+}', function($id) use($app) {

        $user = $app->request->getJsonRawBody();

        $phql = "UPDATE Users SET nick = :nick:, name = :name:, email = :email: WHERE id = :id:";
        $status = $app->modelsManager->executeQuery($phql, array(
            'id' => $id,
            'nick' => $user->nick,
            'name' => $user->name,
            'email' => $user->email
        ));

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($status->success() == true) {
            $response->setStatusCode(201, "Created");
            $user->id = $status->getModel()->id;
            $response->setJsonContent($user);
        } else {

            $response->setStatusCode(409, "Conflict");

            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });

    //--------------------------------------------------------
    // REST API - DELETE /users/{id}
    //-------------------------------------------------------
    $app->delete('/api/users/{id:[0-9]+}', function($id) use ($app) {

        $phql = "DELETE FROM Users WHERE id = :id:";
        $status = $app->modelsManager->executeQuery($phql, array(
            'id' => $id
        ));

        $response = new Phalcon\Http\Response();

        if ($status->success() == true) {
            $response->setStatusCode(204, "No content");
        } else {

            $response->setStatusCode(409, "Conflict");

            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });
/////////////////////////////////////////////////////////////
//-------------------------WORDS---------------------------//
/////////////////////////////////////////////////////////////
//--------------------------------------------------------
// REST API - GET /categorywords/{id}
//-------------------------------------------------------

    $app->get('/api/categorywords/{id:[0-9]+}', function($id) use ($app) {

        $tempCategoryID = $id;
        $phql = "SELECT * FROM Categories WHERE id=:id:";
        $categories = $app->modelsManager->executeQuery($phql, array('id' => $tempCategoryID));
        $data = array();
        $data2 = array();
        foreach ($categories as $category) {
            $phql = "SELECT * FROM Categories WHERE id_parent=:id:";
            $subCategories = $app->modelsManager->executeQuery($phql, array('id' => $category->id));
            $data = findCategories($category->id, $app);

            foreach ($subCategories as $subCategory) {
                $data2 = findCategories($subCategory->id, $app);
            }
        }
        echo json_encode(array_merge($data, $data2));
    });

//
//  SEARCH WORDS
//
    $app->get('/api/search/words/{word:[A-z]+}', function($word) use ($app) {

        $phql = "SELECT W.id, T.cs as id_name, W.img, W.video, W.sign, W.sound, W.default,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name WHERE T.cs LIKE CONCAT(:word:,\"%\")";
        $words = $app->modelsManager->executeQuery($phql, array('word' => $word));

        $data = array();
        foreach ($words as $word) {
            $data[] = array(
                'id' => $word->id,
                'name' => $word->id_name,
                'img' => $word->img,
                'video' => $word->video,
                'sign' => $word->sign,
                'sound' => $word->sound,
                'default' => $word->default,
                'id_creator' => $word->id_creator,
                'id_category' => $word->id_category
            );
        }
        echo json_encode($data);
    });

// REST API - GET /words/{id}
    //-------------------------------------------------------
    $app->get('/api/words/{id:[0-9]+}', function($id) use ($app) {

        $phql = "SELECT W.id, T.cs as id_name, W.img, W.video, W.sign, W.sound, W.default,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name WHERE W.id=:id:";
        $word = $app->modelsManager->executeQuery($phql, array(
                    'id' => $id
                ))->getFirst();

        //echo $word->id_name;

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($word == false) {
            $response->setStatusCode(409, "Conflict");
        } else {
            $response->setStatusCode(200, "Ok");

            $response->setJsonContent(array(
                'id' => $word->id,
                'name' => $word->id_name,
                'img' => $word->img,
                'video' => $word->video,
                'sign' => $word->sign,
                'sound' => $word->sound,
                'default' => $word->default,
                'id_creator' => $word->id_creator,
                'id_category' => $word->id_category
            ));
        }

        return $response;
    });

    $app->get('/api/words', function() use ($app) {

        $phql = "SELECT W.id, T.cs as id_name, W.img, W.video, W.sign, W.sound, W.default,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name";
        $words = $app->modelsManager->executeQuery($phql);

        $data = array();
        foreach ($words as $word) {
            $data[] = array(
                'id' => $word->id,
                'name' => $word->id_name,
                'img' => $word->img,
                'video' => $word->video,
                'sign' => $word->sign,
                'sound' => $word->sound,
                'default' => $word->default,
                'id_creator' => $word->id_creator,
                'id_category' => $word->id_category
            );
        }

        echo json_encode($data);
    });

    //--------------------------------------------------------
    // REST API - POST /words/{id}
    //-------------------------------------------------------

    $app->post('/api/words', function() use ($app) {

        $word = $app->request->getJsonRawBody();

        $phql = "INSERT INTO Words (id_name, img, video, youtube, sound, default, id_creator, id_category) VALUES (:id_name:, :img:, :video:, :youtube:, :sound:, :default:, :id_creator:, :id_category:)";

        $status = $app->modelsManager->executeQuery($phql, array(
            'id_name' => $word->id_name,
            'img' => $word->img,
            'video' => $word->video,
            'sign' => $word->sign,
            'sound' => $word->sound,
            'default' => $word->default,
            'id_creator' => $word->id_creator,
            'id_category' => $word->id_category
        ));


        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');


        if ($status->success() == true) {
            $response->setStatusCode(201, "Created");
            $word->id = $status->getModel()->id;
            $response->setJsonContent($word);
        } else {

            $response->setStatusCode(409, "Conflict");

            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });

    $app->put('/api/words/{id:[0-9]+}', function($id) use($app) {

        $word = $app->request->getJsonRawBody();

        $phql = "UPDATE Words SET id_name = :id_name:,img = :img:,video = :video:,youtube = :youtube:,sound = :sound:,default = :default:,id_creator = :id_creator:, id_category = :id_category: WHERE id = :id:";
        $status = $app->modelsManager->executeQuery($phql, array(
            'id' => $id,
            'id_name' => $word->id_name,
            'img' => $word->img,
            'video' => $word->video,
            'sign' => $word->sign,
            'sound' => $word->sound,
            'default' => $word->default,
            'id_creator' => $word->id_creator,
            'id_category' => $word->id_category
        ));

        $response = new Phalcon\Http\Response();
        $response->setContentType('application/json');

        if ($status->success() == true) {
            $response->setStatusCode(201, "Created");
            $word->id = $status->getModel()->id;
            $response->setJsonContent($word);
        } else {

            $response->setStatusCode(409, "Conflict");

            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });

//--------------------------------------------------------
    // REST API - DELETE /words/{id}
    //-------------------------------------------------------
    $app->delete('/api/words/{id:[0-9]+}', function($id) use ($app) {

        $phql = "DELETE FROM Words WHERE id = :id:";
        $status = $app->modelsManager->executeQuery($phql, array(
            'id' => $id
        ));

        $response = new Phalcon\Http\Response();

        if ($status->success() == true) {
            $response->setStatusCode(204, "No Content");
        } else {

            $response->setStatusCode(409, "Conflict");

            $errors = array();
            foreach ($status->getMessages() as $message) {
                $errors[] = $message->getMessage();
            }

            $response->setJsonContent($errors);
        }

        return $response;
    });
    //--------------------------------------------------------

    $userController = new UsersController();
    $app->get('/api/user/registration', array($userController, "indexAction"));

    $app->post('/api/user/register', array($userController, "registerAction"));

    $sessionController = new SessionController();
    $app->post('/api/user/auth', array($sessionController, "startAction"));

    $app->get('/api/user/role', array($sessionController, "LoggedUserRole"));

    $app->get('/api', array(new IndexController(), "indexAction"));


    $app->handle();
} catch (\Phalcon\Exception $e) {
    echo "PhalconException: ", $e->getMessage();
}

function findCategories($id, $app) {

    $phql = "SELECT T.cs as id_name, W.id, W.img, W.video, W.sign, W.sound, W.default,W.id_creator, W.id_category FROM Words W JOIN Translations T ON T.id=W.id_name WHERE W.id_category=:id:";
    $words = $app->modelsManager->executeQuery($phql, array('id' => $id));

    $data = array();
    foreach ($words as $word) {
        $data[] = array(
            'id' => $word->id,
            'name' => $word->id_name,
            'img' => $word->img,
            'video' => $word->video,
            'sign' => $word->sign,
            'sound' => $word->sound,
            'default' => $word->default,
            'id_creator' => $word->id_creator,
            'id_category' => $word->id_category
        );
    }
    return $data;
}
