<?php
/**
 * Created by PhpStorm.
 * User: Dominik
 * Date: 4.8.14
 * Time: 10:18
 */

class Rhyme extends \Phalcon\Mvc\Model {

	public $id;
	public $id_word;
	public $id_name;
	public $id_rhyme;
	

	public function get($id = null){
		$rhymes =  $this::find("id=$id");
		foreach($rhymes as $rhyme){
			$this->id = $rhyme->id;
			$this->id_word = $this->id_word;
			$this->id_name = $rhyme->id_name;
			$this->id_rhyme = $rhyme->id_rhyme;
			
			return $this->toJson();
		}
	}
	public function post($request){
		return $this->save($this->request->getPost(), array('id_word','id_name','id_rhyme'));
	}
	public function delete($id){
		$rhyme = $this::find("id=$id");
		return	$rhyme->delete();
	}
	public function toJson(){
		return json_encode(array('id'=>$this->id,'id_word'=>$this->id_word,'id_name'=>$this->id_name,'id_rhyme'=>$this->id_rhyme));
	}
}