<?php

class Rhymes extends Phalcon\Mvc\Model
{       
	public $id;
	public $id_name;
	public $id_rhyme;
	public $id_word;
  
    public function getSource()
    {
        return 'rhymes';
    }   
    
    static function getByWord($id_word){
		    $rhyme = Rhymes::findFirst(array(
                        "id_word = :id_word:",
                        "bind" => array('id_word' => $id_word)
                    ));         
        return $rhyme;
    }

}